from .models import Planeta
from .serializers import PlanetaSerializer
from rest_framework import generics
from django_filters import rest_framework as filters
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser, IsAuthenticated, AllowAny

# Responsável por fornecer as propriedades de autenticação.
class Autenticacao:

    authentication_classes = [SessionAuthentication]
    permission_classes = (IsAuthenticated,)

# Responsável por listar e cadastrar planetas.
class PlanetaList(Autenticacao, generics.ListCreateAPIView):

    serializer_class = PlanetaSerializer
    queryset = Planeta.objects.all()

    filter_backends = (filters.DjangoFilterBackend,)
    filter_fields = ('nome',)

# Responsável por atualizar e deletar planetas.
class PlanetaDetail(Autenticacao, generics.RetrieveUpdateDestroyAPIView):

    serializer_class = PlanetaSerializer
    queryset = Planeta.objects.all()
