from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^planetas/$', views.PlanetaList.as_view(), name='planeta-list'),
    url(r'^planetas/(?P<pk>[0-9]+)/$', views.PlanetaDetail.as_view(), name='planeta-detail')
]