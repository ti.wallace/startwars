from django.db import models

"""Model Planeta

Modelo contendo as abstrações dos Planetas da API.

Variáveis;
    Nome {Nome do planeta}
    Clima {Clima do planeta}
    Terreno {Terreno do planeta}
    Aparições {Quantas vezes apareceu nos filmes}
     
"""
class Planeta(models.Model):

    class Meta:

        db_table = 'planetas'

    nome = models.CharField(max_length=200)
    clima = models.CharField(max_length=200)
    terreno = models.CharField(max_length=200)
    aparicoes = models.CharField(max_length=200)

    def __str__(self):
        return self.title