<h1 align="center">Desafio B2W Star Wars</h1>

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/capa.jpeg" alt="Star Wars"></img></h2>

## Descrição

Para este desafio, foi proposto pela B2W o desenvolvimento de uma API Rest que possibilite o time de front-end consultar, cadastrar, atualizar e excluir dados dos planetas da franquia Star Wars, com o intuito de aplicar estes dados no desenvolvimento de um jogo da franquia.

## Linguagem utilizada

- Python (v3.7)

## Tecnologias utilizadas

- Python Django Framework
- Python Django Rest Framework
- Python Django Filter
- SQLite 3

## Tipo de autenticação

- Sessão

## Como instalar a aplicação.

- Crie um ambiente virtual para o projeto.

``` bash
$ mkvirtualenv star_wars
```

- Caso o ambiente não se inicie automaticamente, utilize o comando:

 ``` bash
$ workon star_wars
```

- Instale o Django Framework e o Django Rest Framework.
 
 ``` bash
$ pip install django
```

 ``` bash
$ pip install djangorestframework
```

- Faça o clone do projeto para o seu repositorio local.

 ``` bash
$ git clone https://gitlab.com/ti.wallace/startwars.git
```

- Navegue ate a raiz do diretório do projeto.

``` bash
$ cd patch/to/project/startwars
```

- Instale o modulo Django Filter.

``` bash
$ pip install django-filter
```

- Crie um usuário utilizando o comando:

``` bash
$ python manage.py createsuperuser
```

- Inicie o servidor local utilizando o comando:

``` bash
$ python manage.py runserver
```

## Testando a aplicação.

O Django Rest Framework nos fornece um painel para testarmos a nossa API Rest. Como estamos utilizando o metodo de autenticação por sessão, primeiro precisamos iniciar a mesma. Em seu navegador acesse o link http://127.0.0.1:8000/admin/
A página abaixo devera ser iniciada:

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/login.png" alt="Painel de login"></img></h2>

Realize login para acessar o painel administrativo. Agora a nossa sessão esta iniciada.

Agora acesse o link http://127.0.0.1:8000/planetas/ para ter acesso ao painel de teste.
A página abaixo devera ser iniciada:

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/painel1.png" alt="Painel de Teste - Listagem"></img></h2>

Através do painel do Django Rest Framework podemos realizar os teste na nossa aplicação. Siga os passos a seguir para realizar os teste (GRUD) de nossa API.

- Para listar todos os planetas [GET] clique no botão:

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/painel2.png" alt="Painel de Teste - GET"></img></h2>

Também é possivel buscar por um registro especifico utilizando o Id ou o nome, Para isto, utilize os links:  

http://127.0.0.1:8000/planetas/{Id}

http://127.0.0.1:8000/planetas?nome={nome}

- Para cadastrar um planeta [POST] utilize o formulário:

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/painel3.png" alt="Painel de Teste - POST"></img></h2>

- Para atualizar os dados de um planeta [PUT] acesse o link http://127.0.0.1:8000/planetas/{Id} em seu navegador e atualize os dados no formulário. 

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/painel4.png" alt="Painel de Teste - PUT"></img></h2>

- Para deletar um planeta [DELETE] acesse o link http://127.0.0.1:8000/planetas/{Id} em seu navegador e clique no botão: 

<h2 align="center"><img src="https://gitlab.com/ti.wallace/startwars/raw/master/img/painel5.png" alt="Painel de Teste - DELETE"></img></h2>

## Mapa de acessos Rest

| Rota                    | Verbo    | Parâmetro(s)           | Header                           | Body                                                                 | Descrição                                                                                        |
|-------------------------|----------|------------------------|----------------------------------|----------------------------------------------------------------------|--------------------------------------------------------------------------------------------------|
| `/planetas`             | `GET`    | -                      | -                                | -                                                                    | Lista todos os planetas cadastrados                                                              |
| `/planetas/{id}`        | `GET`    | $id: Int (ObjectID)    | -                                | -                                                                    | Retorna as informações de um planeta em específico a partir de seu ID (ObjectID)                 |
| `/planetas/?nome={name}`| `GET`    | $nome: String          | -                                | -                                                                    | Retorna as informações de um planeta em específico a partir de seu Nome                          |
| `/planetas/`            | `POST`   | -                      | `Content-Type: application/json` | { nome: String, clima: String, terreno: String, aparicoes: String }  | Cadastra um planeta no banco de dados                                                            |
| `/planetas/{id}`        | `PUT`    | -                      | `Content-Type: application/json` | { nome: String, clima: String, terreno: String, aparicoes: String }  | Atualiza um planeta no banco de dados                                                            |
| `/planetas/{id}`        | `DELETE` | -                      | `Content-Type: application/json` | { id: String (ObjectID) }                                            | Deleta um planeta do banco de dados                                                              |